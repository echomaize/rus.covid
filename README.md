# Scary COVID-19 Graphs

Simple data crunching and graphing around COVID-19. In production [here](https://rys.io/covid/).

Before using this for anything serious, read the disclaimers on the site.

## Testing locally

This project is developed in the [PMF methodology](http://programming-motherfucker.com/).

Therefore, there are **no** external dependencies to pull, you **do not** need a local webserver, and you **do not** need `node` to use `npm` to get `yarn` to get `bower` to install `webpack`, because this techie happens to know how to left-pad without [`leftpad`](https://www.theregister.co.uk/2016/03/23/npm_left_pad_chaos/).

**Just clone this repository locally and navigate in your browser to the `index.html` file (for example, `file:///home/user/Projects/covid/index.html`). It should just work. That's it.**

## Deployment

Copy the files to the location they're going to be served from. That's it. It's all just static.

## Privacy

There are no trackers, and no third-party content. This is completely self-contained.

And it is self-contained because there is absolutely zero need for it not to be.

Remember this when somebody tells you again that they need Google Fonts ([nope](https://git.rys.io/rysiek/fonts-degooglifier)), dozens of MiB of JS from 10 different CDNs, and a Facebook log-in button just to make a single page displaying a graph. Because that right there is bullshit.

## ToDo

Some improvements have been suggested, no promises if and when they get implemented!

 - ~~normalize to population, show cases per million~~
 - ~~make the new daily cases graph a rolling average with control over how many days/datapoints~~
 - ~~chart global cases~~
 - chart cases requiring intensive medical care
 - chart tested
 - ~~chart deaths~~

 
## FAQ

 - ***Wy does the data start on January 22nd, 2020?***  
   I'm using [John Hopkins University dataset](https://github.com/CSSEGISandData/COVID-19) (via [`pomber/covid19`](https://github.com/pomber/covid19)), and that's when it starts. I don't see any specific reason in the [WHO Timeline](https://www.who.int/news-room/detail/08-04-2020-who-timeline---covid-19), either:

>  22 January 2020
> 
> WHO mission to China issued a statement saying that there was evidence of human-to-human transmission in Wuhan but more investigation was needed to understand the full extent of transmission.

 - ***Some things do not work in Safari. Why?***  
   Because [Safari is broken](https://bugs.webkit.org/show_bug.cgi?id=119175), that's why.

 - ***Logarithmic scale does not properly show negative numbers!***  
   That's [true](https://canvasjs.com/forums/topic/negative-values-with-logarithmic-scale/), and [very hard to work around](https://canvasjs.com/forums/topic/negative-values-with-logarithmic-scale/)
