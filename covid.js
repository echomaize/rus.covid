/*
 * available sites array and site-to-data mapping
 */
var sites = []

/*
 * yes, population is hard-coded
 * doesn't change that much, removes one failure mode, and removes one request to Wikipedia
 *
 * for new infection sites machinery of getting population from WP remains in-place
 */
var siteData = {
    "Global": {
        "population": 7713468000
    },
    "Afghanistan": {
        "population": 32225560
    },
    "Albania": {
        "population": 2876591
    },
    "Algeria": {
        "population": 43000000
    },
    "Andorra": {
        "population": 76177
    },
    "Angola": {
        "population": 25789024
    },
    "Antigua and Barbuda": {
        "population": 81799
    },
    "Argentina": {
        "population": 44938712
    },
    "Armenia": {
        "population": 3018854
    },
    "Australia": {
        "population": 23401892
    },
    "Austria": {
        "population": 8902600
    },
    "Azerbaijan": {
        "population": 10027874
    },
    "Bahamas": {
        "population": 351461
    },
    "Bahrain": {
        "population": 1234571
    },
    "Bangladesh": {
        "population": 149772364
    },
    "Barbados": {
        "population": 277821
    },
    "Belarus": {
        "population": 9491800
    },
    "Belgium": {
        "population": 11515793
    },
    "Belize": {
        "population": 408487
    },
    "Benin": {
        "population": 10008749
    },
    "Bhutan": {
        "population": 741700
    },
    "Bolivia": {
        "population": 11428245
    },
    "Bosnia and Herzegovina": {
        "population": 3511372
    },
    "Botswana": {
        "population": 2254068
    },
    "Brazil": {
        "population": 210147125
    },
    "Brunei": {
        "population": 442400
    },
    "Bulgaria": {
        "population": 7000039
    },
    "Burkina Faso": {
        "population": 20107509
    },
    "Burundi": {
        "population": 11745876
    },
    "Cambodia": {
        "population": 15288489
    },
    "Cameroon": {
        "population": 17463836
    },
    "Canada": {
        "population": 37894799
    },
    "Cabo Verde": {
        "population": 543767
    },
    "Central African Republic": {
        "population": 4987640
    },
    "Chad": {
        "population": 13670084
    },
    "Chile": {
        "population": 17574003
    },
    "China": {
        "population": 1339724852
    },
    "Colombia": {
        "population": 50372424
    },
    "Congo (Brazzaville)": {
        "population": 5244359
    },
    "Congo (Kinshasa)": {
        "population": 91931000
    },
    "Costa Rica": {
        "population": 4999441
    },
    "Croatia": {
        "population": 4076246
    },
    "Cuba": {
        "population": 11209628
    },
    "Cyprus": {
        "population": 838897
    },
    "Czechia": {
        "population": 10649800
    },
    "Denmark": {
        "population": 5822763
    },
    "Diamond Princess": {
        "population": 3711
    },
    "Djibouti": {
        "population": 921804
    },
    "Dominica": {
        "population": 71293
    },
    "Dominican Republic": {
        "population": 10735896
    },
    "East Timor": {
        "population": 1183643
    },
    "Ecuador": {
        "population": 17300000
    },
    "Egypt": {
        "population": 100075480
    },
    "El Salvador": {
        "population": 6420746
    },
    "Equatorial Guinea": {
        "population": 1222442
    },
    "Eritrea": {
        "population": 5750433
    },
    "Estonia": {
        "population": 1328360
    },
    "Eswatini": {
        "population": 1093238
    },
    "Ethiopia": {
        "population": 73750932
    },
    "Fiji": {
        "population": 926276
    },
    "Finland": {
        "population": 5521158
    },
    "France": {
        "population": 67022000
    },
    "Gabon": {
        "population": 2119275
    },
    "Gambia": {
        "population": 2051363
    },
    "Georgia": {
        "population": 3723464
    },
    "Germany": {
        "population": 83149300
    },
    "Ghana": {
        "population": 31072940
    },
    "Greece": {
        "population": 10768477
    },
    "Grenada": {
        "population": 111454
    },
    "Guinea-Bissau": {
        "population": 1874303
    },
    "Guatemala": {
        "population": 17263239
    },
//     "Guernsey": {
//         "population": 63026
//     },
    "Guinea": {
        "population": 11628972
    },
    "Guyana": {
        "population": 786391
    },
    "Haiti": {
        "population": 11123178
    },
    "Honduras": {
        "population": 8303771
    },
    "Hong Kong": {
        "population": 7482500
    },
    "Hungary": {
        "population": 9772756
    },
    "Iceland": {
        "population": 364260
    },
    "Ireland": {
        "population": 4921500
    },
    "India": {
        "population": 1210854977
    },
    "Indonesia": {
        "population": 237641326
    },
    "Iran": {
        "population": 83183741
    },
    "Iraq": {
        "population": 40194216
    },
    "Isle of Man": {
        "population": 83314
    },
    "Israel": {
        "population": 7412200
    },
    "Italy": {
        "population": 60317546
    },
    "Ivory Coast": {
        "population": 23740424
    },
    "Jamaica": {
        "population": 2890299
    },
    "Japan": {
        "population": 126150000
    },
//     "Jersey": {
//         "population": 106800
//     },
    "Jordan": {
        "population": 10407793
    },
    "Kazakhstan": {
        "population": 18448600
    },
    "Kenya": {
        "population": 47564296
    },
    "Kosovo": {
        "population": 1810463
    },
    "Kuwait": {
        "population": 4621638
    },
    "Kyrgyzstan": {
        "population": 6389500
    },
    "Laos": {
        "population": 7096376
    },
    "Latvia": {
        "population": 1919968
    },
    "Lebanon": {
        "population": 6859408
    },
    "Liberia": {
        "population": 4809768
    },
    "Libya": {
        "population": 6653210
    },
    "Liechtenstein": {
        "population": 38557
    },
    "Lithuania": {
        "population": 2794329
    },
    "Luxembourg": {
        "population": 613894
    },
    "Macau": {
        "population": 667400
    },
    "Madagascar": {
        "population": 26262313
    },
    "Malaysia": {
        "population": 32772100
    },
    "Malawi": {
        "population": 18143217
    },
    "Maldives": {
        "population": 392473
    },
    "Mali": {
        "population": 19329841
    },
    "Malta": {
        "population": 493559
    },
    "Mauritania": {
        "population": 3537368
    },
    "Mauritius": {
        "population": 1265985
    },
    "Mexico": {
        "population": 126577691
    },
    "Moldova": {
        "population": 2681735
    },
    "Monaco": {
        "population": 38300
    },
    "Mongolia": {
        "population": 3278290
    },
    "Montenegro": {
        "population": 631219
    },
    "Morocco": {
        "population": 35581294
    },
    "Mozambique": {
        "population": 27909798
    },
    "MS Zaandam": {
        "population": 1829
    },
    "Myanmar": {
        "population": 53582855
    },
    "Namibia": {
        "population": 2606971
    },
    "Nepal": {
        "population": 26494504
    },
    "Netherlands": {
        "population": 17424978
    },
    "New Zealand": {
        "population": 4977520
    },
    "Nicaragua": {
        "population": 6167237
    },
    "Niger": {
        "population": 17138707
    },
    "Nigeria": {
        "population": 204630242
    },
    "North Macedonia": {
        "population": 2077132
    },
//     "Northern Cyprus": {
//         "population": 326000
//     },
    "Norway": {
        "population": 5367580
    },
    "Oman": {
        "population": 2773479
    },
    "Pakistan": {
        "population": 212228286
    },
    "Palestine": {
        "population": 4952168
    },
    "Panama": {
        "population": 3405813
    },
    "Papua New Guinea": {
        "population": 8606323
    },
    "Paraguay": {
        "population": 7152703
    },
    "Peru": {
        "population": 32824358
    },
    "Philippines": {
        "population": 100981437
    },
    "Poland": {
        "population": 38386000
    },
    "Portugal": {
        "population": 10276617
    },
    "Qatar": {
        "population": 2641669
    },
    "Romania": {
        "population": 19401658
    },
    "Russia": {
        "population": 146745098
    },
    "Rwanda": {
        "population": 12374397
    },
    "Saint Kitts and Nevis": {
        "population": 46204
    },
    "Saint Lucia": {
        "population": 165595
    },
    "Sakhalin": {
        "population": 497973
    },
    "San Marino": {
        "population": 33344
    },
    "Sao Tome and Principe": {
        "population": 211028
    },
    "Saudi Arabia": {
        "population": 34218169
    },
    "Senegal": {
        "population": 14668522
    },
    "Serbia": {
        "population": 6963764
    },
    "Seychelles": {
        "population": 97096
    },
    "Sierra Leone": {
        "population": 7092113
    },
    "Singapore": {
        "population": 5638700
    },
    "Slovakia": {
        "population": 5450421
    },
    "Slovenia": {
        "population": 2094060
    },
    "Somalia": {
        "population": 11031386
    },
    "South Africa": {
        "population": 58775022
    },
    "South Korea": {
        "population": 51709098
    },
    "South Sudan": {
        "population": 10975927
    },
    "Spain": {
        "population": 46733038
    },
    "Sri Lanka": {
        "population": 21670000
    },
    "St. Vincent and the Grenadines": {
        "population": 109991
    },
    "Sudan": {
        "population": 41592539
    },
    "Suriname": {
        "population": 541638
    },
    "Sweden": {
        "population": 10333456
    },
    "Switzerland": {
        "population": 8570146
    },
    "Syria": {
        "population": 17070135
    },
    "Taiwan": {
        "population": 23780452
    },
    "Tanzania": {
        "population": 44928923
    },
    "Thailand": {
        "population": 64785909
    },
    "Togo": {
        "population": 7965055
    },
    "Trinidad and Tobago": {
        "population": 1363985
    },
    "Tunisia": {
        "population": 11722038
    },
    "Turkey": {
        "population": 83154997
    },
    "Uganda": {
        "population": 34634650
    },
    "Ukraine": {
        "population": 42030832
    },
    "United Arab Emirates": {
        "population": 9599353
    },
    "United Kingdom": {
        "population": 67545757
    },
    "United States": {
        "population": 328239523
    },
    "Uruguay": {
        "population": 3390077
    },
    "Uzbekistan": {
        "population": 33961708
    },
    "Vatican City": {
        "population": 1000
    },
    "Venezuela": {
        "population": 28887118
    },
    "Vietnam": {
        "population": 95545962
    },
    "Western Sahara": {
        "population": 567402
    },
    "Yemen": {
        "population": 28498683
    },
    "Zambia": {
        "population": 13092666
    },
    "Zimbabwe": {
        "population": 12973808
    }
}

/*
 * get the global pandemic data
 */
let getCovidData = () => {
    console.log(`+-- fetching global covid data`)

    return fetch("https://meduza.io/api/g4/covid_data/regional_russian_map_data")

            .then(response => response.json())
            .then((meduza_data) => {
                meduza_data = meduza_data.data_by_location;
                date_key = Object.keys(meduza_data[0].data).toString();
                daily_data = meduza_data[0].data[date_key]
                cnf = daily_data["cnf"]
                dth = daily_data["dth"]
                rcv = daily_data["rcv"]

                Object
                .keys(meduza_data)
                .forEach(region => {
                  region = meduza_data[region].title;
                  data = {
                    [region]:[
                      {
                        "date": date_key,
                        "confirmed": cnf,
                        "deaths":dth,
                        "recovered":rcv
                      }]
                    };
                    console.log(data)
                  })


                siteData['Global'].data = Object.values(data)
                    .reduce((acc, cur, idx, arr)=>{
                        cur.map((curcur, idxidx)=>{
                            if (! (idxidx in acc)) {
                                acc[idxidx] = {
                                    date: curcur.date,
                                    confirmed: 0,
                                    deaths: 0,
                                    recovered: 0
                                }
                            }
                            acc[idxidx].confirmed += curcur.confirmed
                            acc[idxidx].deaths += curcur.deaths
                            acc[idxidx].recovered += curcur.recovered
                        })
                        return acc
                    }, [])

                // Global data needs a source too!
                siteData['Global'].source_link = "https://github.com/pomber/covid19"
                siteData['Global'].source_title = "pomber / covid19"

                var cleaned_data = siteData['Global'].data.slice(-1)[0]
                cleaned_data.death_to_case = cleaned_data.deaths / cleaned_data.confirmed
                console.log(`+-- got global covid data:`
                        + `\n    date          : ${cleaned_data.date}`
                        + `\n    confirmed     : ${cleaned_data.confirmed}`
                        + `\n    deaths        : ${cleaned_data.deaths}`
                        + `\n    recovered     : ${cleaned_data.recovered}`
                        + `\n    death-to-case : ${cleaned_data.death_to_case}`)

                // hey we can get countries / locations / infection sites list from this too!
                Object
                    .keys(data)
                    .forEach((cur)=>{
                        // snowflakes
                        if (cur === 'US') {
                            site = 'United States';
                        } else if (cur === 'Taiwan*') {
                            site = 'Taiwan'
                        } else if (cur === "Cote d'Ivoire") {
                            site = 'Ivory Coast'
                        } else if (cur === 'Holy See') {
                            site = 'Vatican City'
                        } else if (cur === 'Korea, South') {
                            site = 'South Korea'
                        } else if (cur === 'Saint Vincent and the Grenadines') {
                            site = 'St. Vincent and the Grenadines'
                        } else if (cur === 'Timor-Leste') {
                            site = 'East Timor'
                        } else if (cur === 'West Bank and Gaza') {
                            site = 'Palestine'
                        } else if (cur === 'Burma') {
                            site = 'Myanmar'
                        } else {
                            site = cur
                        }
                        if (! (site in siteData)) {
                            siteData[site] = {}
                        }
                        siteData[site].data = data[cur]
                        siteData[site].source_link = "https://github.com/pomber/covid19"
                        siteData[site].source_title = "pomber / covid19"
                    })
                sites = Object.keys(siteData).sort()
                // we want Global separated by the dashes from other sites
                sites.splice(sites.indexOf('Global'), 1);
                sites.unshift('Global', '- - - - - - - - - - - - - - - - - - - - - - - -')

                return cleaned_data
            })
}


/*
 * calculate the deltas for a COVID data series
 */
let calculateDeltas = (data) => {
    // perhaps we have that data already?
    if ("delta_confirmed" in data.data[0]) {
        return data;
    }
    // calculate the deltas
    data.data = data.data.map((cur, i, d)=>{

            if (i==0) {
                cur.delta_confirmed = 0;
                cur.delta_recovered = 0;
                cur.delta_deaths = 0;
                cur.active = 0;
                cur.delta_active = 0;
                cur.cfr = 0
                cur.delta_cfr = 0

            } else {
                cur.delta_confirmed = cur.confirmed - d[i-1].confirmed
                cur.delta_recovered = cur.recovered - d[i-1].recovered
                cur.delta_deaths = cur.deaths - d[i-1].deaths

                var removed = cur.recovered + cur.deaths
                cur.active = cur.confirmed - removed
                cur.delta_active = cur.active - d[i-1].active

                // case fatality rate
                if (removed > 0) {
                    cur.cfr = cur.deaths / removed
                    cur.delta_cfr = cur.cfr - d[i-1].cfr
                } else {
                    cur.cfr = 0
                    cur.delta_cfr = 0
                }
            }
            return cur;
        })
    return data;
}


/*
 * get (and clean up) the cases data for a given outbreak site
 *
 * returns a promise that resolves to the an array of objects, each containing multiple keys, including:
 * - date
 * - confirmed
 * (if all goes well, that is)
 */
let getSiteCases = (site) => {

    // if we have the data, use that
    if ( ( site in siteData )
      && ( 'data' in siteData[site] ) ) {
        console.log(`+-- using cached covid data for ${site}`)
        return Promise.resolve(siteData[site]).then((data)=>{ return calculateDeltas(data) });
    }

    // snowflake templates are snowflake
    request_site = site
    if (site === "China") {
        request_site = "Mainland China"
    } else if (site === "Czechia") {
        request_site = "The Czech Republic"
    }
    console.log(`+-- fetching covid data for: ${site} (as: ${request_site})`)
    return wtf
        .fetch(`Template:2019-20 coronavirus pandemic data/${request_site} medical cases chart`)
        .then((doc)=>{
            console.log(`+-- got covid data for: ${site} (as: ${request_site})`)
            tdata = doc.json()
            // this works for most templates
            var data = tdata
                .sections[0]
                .templates
                .filter(row => ( (row.template == 'medical cases chart/row') && (row.list[0] != "") ) )
                .map((row)=>{
                    if (row.list[6] !== "") {
                        confirmed = 1 * row.list[6].replace(/[^0-9]/g, '')
                    } else {
                        confirmed = 1 * row.list[8].replace(/[^0-9]/g, '')
                    }
                    return {
                        date: row.list[0],
                        confirmed: confirmed
                    }
                })

            if (data.length > 0) {
                console.log(`    strategy 1 succeeded for ${site}`)

            // some templates are done in a different way
            } else {
                data = tdata
                    .sections[0]
                    .templates
                    .filter(row => ( ( row.template == 'bar stacked' ) && (row.list[0] != "") ) )
                    .map((row)=>{
                        return {
                            date: row.list[0],
                            confirmed: 1 * row
                                        .list[1]
                                        .replace(/^([0-9,]+).*$/, '$1')
                                        .replace(/,/g, '')
                        }
                    })
                if (data.length > 0) {
                    console.log(`    strategy 2 succeeded for ${site}`)
                }
            }

            // still no data? probably fscking Mainland China
            if (data.length === 0) {
                tempdata = tdata
                    .sections[0]
                    .templates
                    .filter(tmpl => tmpl.template === 'medical cases chart')

                if (tempdata.length > 0) {
                    data = tempdata[0]
                        .data
                        .split('\n')
                        .map(row=>row.split(';'))
                        .filter(row=> row[0] != "")
                        .map((row) => {
                            if (row[6] !== "") {
                                confirmed = 1 * row[6].replace(/[^0-9]/g, '')
                            } else if (row[7] !== "") {
                                confirmed = 1 * row[7].replace(/[^0-9]/g, '')
                            } else {
                                confirmed = 1 * row[8].replace(/[^0-9]/g, '')
                            }
                            return {
                                date: row[0],
                                confirmed: confirmed
                            }
                        })
                }

                if (data.length > 0) {
                    console.log(`    strategy 3 succeeded for ${site}`)
                }
            }

            // *still* no data?! probably fscking Netherlands
            if (data.length===0) {
                data = tdata
                    .sections[0]
                    .paragraphs
                    .map(p=>p.sentences)
                    .flat()
                    .filter(row=>row.text.slice(0,25)==='{{Medical cases chart/Row')
                    .map(row => row.text.split('|').slice(1))
                    .filter(row=> row[0] != "")
                    .map((row) => {
                        if (row[6] !== "") {
                            confirmed = 1 * row[6].replace(/[^0-9]/g, '')
                        } else if (row[7] !== "") {
                            confirmed = 1 * row[7].replace(/[^0-9]/g, '')
                        } else {
                            confirmed = 1 * row[8].replace(/[^0-9]/g, '')
                        }
                        return {
                            date: row[0],
                            confirmed: confirmed
                        }
                    })

                if (data.length > 0) {
                    console.log(`    strategy 4 succeeded for ${site}`)
                }
            }

            if (data.length === 0) {
                console.log(`    ERROR: no strategy succeeded for ${site}`)
                return false
            }

            // we need to fill the gaps in places
            var gaps_filled = [data[0]]
            var cur_date = new Date(data[0].date)
            var cur_index = 1 // starting from one, yes

            // failsafe in case this is a runaway loop
            var failsafe = 500

            // go through the data and fill in the blanks
            while ( (cur_index < data.length) && (failsafe > 0) ) {
                // decrement the failsafe
                failsafe -= 1
                // get the date
                var data_date = new Date(data[cur_index].date)
                // check if it's a valid date
                if (!isNaN(data_date)) {
                    // increment the date
                    cur_date.setDate(cur_date.getDate() + 1)
                    // check if it matches
                    if (data_date <= cur_date) {
                        gaps_filled.push(data[cur_index])
                        cur_index += 1
                    } else {
                        // presumably we're in a gap!
                        gaps_filled.push({
                            date: `${cur_date.getFullYear()}-${("0" + (cur_date.getMonth() + 1)).slice(-2)}-${("0" + cur_date.getDate()).slice(-2)}`,
                            confirmed: data[cur_index-1].confirmed
                        })
                    }
                // if not a valid date, just ignore and move on
                } else {
                    cur_index += 1
                }
            }

            if (failsafe === 0) {
                console.log('ERROR: data gaps filling loop was killed! using unfixed data!')
                gaps_filled = data
            }

            // we have everything
            site_data = {
                data: gaps_filled,
                source_link: `https://en.wikipedia.org/?curid=${tdata.pageID}`,
                source_title: tdata.title
            }

            // cache the cases data in case we need it later
            if ( ! ( site in siteData ) ) {
                siteData[site] = {}
            }
            siteData[site] = { ...siteData[site], ...site_data }
            // return it
            return site_data
        })
        // calculate the deltas
        .then((data)=>{ return calculateDeltas(data) });
}


/*
 * get population data of a Wikipedia-recognized site
 *
 * returns a promise that resolves to integer (if all goes well)
 */
let getSitePopulation = (site) => {

    // if we have the data, use that
    if ( ( site in siteData )
      && ( 'population' in siteData[site] ) ) {
        console.log(`+-- using cached population data for ${site}`)
        return Promise.resolve(siteData[site].population);
    }

    // snowflake site pages are snowflake
    request_site = site
    if (site === "Georgia") {
        request_site = "Georgia (country)"
    }
    console.log(`+-- fetching population data for ${site} (as: ${request_site})`)

    return wtf
        .fetch(request_site)
        .then((doc)=>{
            infobox = doc.json().sections[0].infoboxes[0]
            if (`population_estimate` in infobox) {
                var pop_data = infobox.population_estimate
            } else if (`population_census` in infobox) {
                var pop_data = infobox.population_census
            }
            var population = false
            if ('number' in pop_data) {
                population = pop_data.number
            } else {
                population = pop_data.text.split(/\n/g)[0].replace(/^([0-9,]+).*$/g, '$1').replace(/,/g, '')
            }
            console.log(`+-- got population data for ${site} (as: ${request_site}):`
                    + `\n    population: ${population}`)

            // cache the population data in case we need it later
            if ( ! ( site in siteData ) ) {
                siteData[site] = {}
            }
            siteData[site].population = population
            // return it
            return population
        })
}


/*
 * generating an array with values
 */
let genArr = (start, length, step) => {
    var arr = []
    arr[0] = start
    for (var i=1; i<length; i++) {
        if (typeof step === 'function') {
            arr[i] = step(i, arr)
        } else {
            arr[i] = arr[i-1] + step
        }
    }
    return arr
}


/*
 * calculating the rate of infection on a site
 * averaging over all available datapoints
 *
 * getting data averaged over the last 5 datapoints is as simple as
 * calculateAverageRate(data.slice(-5))
 */
let calculateAverageRate = (data) => {
  var a = data.reduce((acc, el, i, d)=>{
    if (i==0) return acc;
    acc += el / d[i-1];
    return acc
  }, 0)
  return a / (data.length - 1)
}


/*
 * handling a .sites-select change
 */
let selectSite = (e) => {

    var theSelect = e.target

    // reality check
    if (! (theSelect.value in siteData)) {
        return false
    }

    clearTimeout(theSelect.selectTimeout)
    theSelect.selectTimeout = false

    var siteDataNode = theSelect.parentElement.parentElement.getElementsByClassName("site-data")[0];

    // need to handle the corner cases, eh
    var site = theSelect.value

    // using a timeout to give the user a chance to change their mind
    theSelect.selectTimeout = setTimeout(function(){
        theSelect.selectTimeout = false
        theSelect.fetching = true
        console.log(`+-- fetching data for: ${site}`)
        siteDataNode.innerHTML = `<div class="please-wait">Fetching data, please wait...</div>`
        getSiteCases(site)
            .then((wikiData)=>{
                var data = wikiData.data
                if (! ("ratio" in data[data.length-1]) ) {
                    data[data.length-1].ratio = calculateAverageRate(data.slice(-4).map(d=>d.confirmed))
                }
                var confirmed = data[data.length-1].confirmed
                console.log(`+-- got covid data for: ${site}`)
                getSitePopulation(site)
                    .then((population)=>{
                        siteDataNode.innerHTML = `
                            <div><span class="label">Population:</span><span class="value">${population}</span></div>
                            <div><span class="label">Cases:</span><span class="value">${confirmed}</span></div>
                            <div><span class="label">% infected:</span><span class="value">${Math.round((confirmed/population)*10000)/100}%</span></div>
                            <div><span class="label">Rate:</span><span class="value">${Math.round((data[data.length-1].ratio - 1) * 10000) / 100}%</span></div>
                            <div><span class="label">Doubles every:</span><span class="value">${Math.round(Math.log(2)/Math.log(data[data.length-1].ratio))} days</span></div>
                            <div><span class="label">Half infected in:</span><span class="value">${Math.round(Math.log(population*0.5/confirmed)/Math.log(data[data.length-1].ratio))} days</span></div>
                            <div class="sources"><a href="${wikiData.source_link}">source</a></div>`
                        theSelect.fetching = false
                        updateChartData(theSelect)
                    })
                    .catch((e)=>{
                        theSelect.fetching = false
                        console.log(e)
                        siteDataNode.innerHTML = `<div class="error">Something went wrong. Either a bug, or no data.</div>`
                    })
            })
            .catch((e)=>{
                theSelect.fetching = false
                console.log(e)
                siteDataNode.innerHTML = `<div class="error">Something went wrong. Either a bug, or no data.</div>`
            })
    }, 500)
}


/*
 * create the chart's guideline gradient for a dataset
 * dataset - the index of the dataset to work with
 */
let chartGuidelineGradient = (chart, dataset) => {

    // if the dataset is hidden, we don't need to do anythin
    if (chart.data.datasets[dataset].hidden) {
        return false;
    }

    // get the metadata of the chart dataset
    // that is, x, y coordinates of points! yay!
    var meta = chart.getDatasetMeta(dataset);
    if (meta.data.length === 0) {
        return true;
    }
    // choose a point pls
    if (chart.options.scales.yAxes[0].type === 'linear') {
        var p1 = meta.data.length - 1
        while (meta.data[p1]._model.y < chart.chartArea.top) {
            p1 -= 1
        }
        p1 -= 1
    } else {
        var p1 = 1
    }

    // get the coords of the point, and of the next point
    var x = meta.data[p1]._model.x;
    var y = meta.data[p1]._model.y;
    var x2 = meta.data[p1+1]._model.x;
    var y2 = meta.data[p1+1]._model.y;

    // calculate the deltas
    var dx = x2 - x
    var dy = y2 - y

    // what are the dimensions of the chart?
    var cw = chart.chartArea.bottom - chart.chartArea.top
    var ch = chart.chartArea.right - chart.chartArea.left
    // let's get half of the lowest dimension
    var cd  = Math.min(cw, ch) / 2

    // dratio² × dy² + dratio² × dx² = cd²
    dratio = Math.sqrt(cd*cd / (dy * dy + dx * dx))

    // create the gradient, using p1's coords as a starting point,
    // and swapped deltas to get a perpendicular vector
    var gradient = chart.ctx.createLinearGradient(x, y, x - dratio * dy, y + dratio * dx);

    // get the color we're working with
    var guide_color = chart.data.datasets[dataset].borderColor.replace(/^rgba\(([0-9]+, [0-9]+, [0-9]+), (.+)\)$/g, '$1|$2').split('|')
    // assumption: in the gradient we're only working with opacity, and opacity is a float
    var guide_opacity = 1 * guide_color[1]
    guide_color = guide_color[0]

    // set up the gradient
    gradient.addColorStop(0, chart.data.datasets[dataset].borderColor);
    gradient.addColorStop(0.2, `rgba(${guide_color}, ${guide_opacity/2})`);
    gradient.addColorStop(0.5, `rgba(${guide_color}, ${guide_opacity/4})`);
    gradient.addColorStop(1, `rgba(${guide_color}, 0)`);

    // set the gradient as a backgroundColor for the dataset
    chart.data.datasets[dataset].backgroundColor = gradient
}


var theChart = false;
let chart = () => {
    var context = document.getElementById('the-chart').getContext('2d');
    var graph_width = 45;
    Chart.defaults.global.defaultFontColor = '#bbb';
    Chart.defaults.global.defaultFontFamily = 'Play Regular';
    theChart = new Chart(context, {
        type: 'line',
        data: {
            datasets: [{
                label: 'Doubles every day',
                data: genArr(1, graph_width, (i, arr)=>{ return arr[i-1]*2 }),
                backgroundColor: 'rgba(127, 64, 64, 0.15)',
                borderColor: 'rgba(127, 64, 64, 0.15)',
                pointRadius: 0,
                borderWidth: 1,
                lineTension: 0.1,
                fill: 1
            }, {
                label: 'Doubles every 2 days',
                data: genArr(1, graph_width, (i, arr)=>{ return arr[i-1]*Math.pow(2, 1/2) }),
                backgroundColor: 'rgba(176, 132, 81, 0.15)',
                borderColor: 'rgba(176, 132, 81, 0.15)',
                pointRadius: 0,
                borderWidth: 1,
                lineTension: 0.1,
                fill: 2
            }, {
                label: 'Doubles every 7 days',
                data: genArr(1, graph_width, (i, arr)=>{ return arr[i-1]*Math.pow(2, 1/7) }),
                backgroundColor: 'rgba(182, 182, 81, 0.15)',
                borderColor: 'rgba(182, 182, 81, 0.15)',
                pointRadius: 0,
                borderWidth: 1,
                lineTension: 0.1,
                fill: "origin"
            }],
            labels: genArr(1, graph_width, 1)
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                labels: {
                    // This more specific font property overrides the global property
                    fontSize: 16,
                    // hide the legend entries for guidelines
                    filter: (legendItem, data) => {
                        return (legendItem.datasetIndex > 2);
                    }
                }
            },
            title: {
                display: true,
                fontSize: 18,
                lineHeight: 1.4,
                text: "COVID-19 infections"
            },
            scales: {
                yAxes: [{
                    type: "logarithmic",
                    scaleLabel: {
                        display: true,
                        fontSize: 14,
                        labelString: "Confirmed cases",
                    },
                    ticks: {
                        beginAtZero: true,
                        fontSize: 9,
                        autoSkip: true,
                        maxTicksLimit: 20,
                        callback: function(value, index, values) {
                            if (value === 0) {
                                return "0";
                            } else if (value % 1000000 == 0) {
                                return (value / 1000000) + 'm';
                            } else if (value % 1000 == 0) {
                                return (value / 1000) + 'k';
                            } else {
                                return value;
                            }
                        }
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        fontSize: 14,
                        labelString: "Days since 1st confirmed case"
                    }
                }]
            }
        },
        plugins: [{
            afterUpdate: function(chart, options) {
                // if we just updated, let's leave it at that
                if (chart.justUpdated) {
                    chart.justUpdated = false
                    return true
                }
                // fix the gradients
                chartGuidelineGradient(chart, 0)
                chartGuidelineGradient(chart, 1)
                chartGuidelineGradient(chart, 2)
                // make sure we don't end up in an update loop
                chart.justUpdated = true
                // update!
                chart.update()
            }
        }]
    });

    // create a secondary legend for the guidelines only
    var legend_opts = {...theChart.options.legend}
    // deep object cloning is not a thing in JS apparently
    legend_opts.labels = {...theChart.options.legend.labels}
    // font size
    legend_opts.labels.fontSize = 13
    legend_opts.labels.usePointStyle = true
    // make sure we're using the guideline datasets only
    legend_opts.labels.filter = (legendItem, data) => {
            return ( (! legendItem.hidden) && (legendItem.datasetIndex <= 2) );
        }
    // use the borderColor for the box inside, instead of the gradient
    // (which might be barely visible at that point of canvas)
    legend_opts.labels.generateLabels = (chart) => {
        var generated_labels = chart.options.legend.labels.generateLabels(chart)
        return generated_labels.map((cur_label, index)=>{
            // get the color we're working with
            var label_color = chart.data.datasets[index].borderColor.replace(/^rgba\(([0-9]+, [0-9]+, [0-9]+), (.+)\)$/g, '$1|$2').split('|')
            var label_opacity = 1 * label_color[1]
            if (label_opacity < 0.5) {
                label_opacity = 0.5
            }
            label_color = label_color[0]
            cur_label.fillStyle = `rgba(${label_color}, ${label_opacity})`
            return cur_label
        })
    }



    // create a new Legend
    var legend = new Chart.Legend({
        ctx: theChart.chart.ctx,
        options: legend_opts,
        chart: theChart
    });

    // a bit of breathing space between it and the chart please
    legend.afterFit = function() {
        this.height = this.height + 20;
    };

    // do the magic
    theChart.newLegend = legend;
    Chart.layoutService.addBox(theChart, legend);
    theChart.update()

    // prepare the new_datasets object
    theChart.data.new_datasets = []

    // set the update timeout thingy
    theChart.updateTimeout = false
}


/*
 * the colors to use for charts
 * blatantly stolen from Chart.js documentation
 */
let niceChartColors = [
    'rgba(255, 99, 132, 0.8)',
    'rgba(54, 162, 235, 0.8)',
    'rgba(255, 206, 86, 0.8)',
    'rgba(75, 192, 192, 0.8)',
    'rgba(153, 102, 255, 0.8)',
    'rgba(255, 159, 64, 0.8)'
]

/*
 * updating le charte
 */
let updateChartData = (siteSelect) => {

    // make sure to cancel any other updates to the chart
    clearTimeout(theChart.updateTimeout)
    theChart.updateTimeout = false

    // do we have any data to work with?
    if (siteSelect.value in siteData) {

        // add the data to the chart
        var to_chart = {
            label: siteSelect.value,
            fill: false,
            borderColor: niceChartColors[siteSelect.siteNo],
            borderWidth: 1,
            lineTension: 0.1
        }

        // which data are we looking at
        var dataset = document.querySelector('input[type=radio][name=chart-data]:checked').value

        // population_ratio is also used as an indicator that we're doing a per-million chart
        var population_ratio = false
        if ( (dataset !== 'cfr') && document.querySelector('input[type=radio][name=chart-values]:checked').value === 'permillion') {
            population_ratio = (siteData[siteSelect.value].population / 1000000)
        }

        // where do we start with the data?
        var start_at = document.querySelector('input[type=radio][name=chart-start]:checked').value
        var start_at_date = false

        if (start_at === 'date') {
            start_at_date = document.querySelector('input[type=date][name=chart-start-from]').value.replace(/-0/g, '-')
        } else if (population_ratio) {
            start_at = start_at * population_ratio
        }

        // dataset name used for start-at comparisons
        if ( (dataset === 'cfr') || (dataset === 'active') ) {
            // when showing CFR data, we want to start at nth confirmed case
            // since CFR by definition cannot go above 1
            var start_at_dataset = 'confirmed'

        // for everything else we're fine with the actual dataset
        } else {
            var start_at_dataset = dataset
        }

        // filter the data
        var found_start = false
        filtered_data = siteData[siteSelect.value]
            .data
            .filter((row, idx) => {
                if (found_start) {
                    return true
                } else if ( ( (start_at_date) && ( row.date === start_at_date) ) || (row[start_at_dataset] >= start_at) ) {
                    found_start = true
                    return true
                }
                return false
            })

        // use cumulative case numbers
        if (document.querySelector('input[type=radio][name=chart-cases]:checked').value === 'cumulative') {
            to_chart.data = filtered_data.map(row => row[dataset])
        // use new cases count
        } else {
            to_chart.data = filtered_data.map(row => row[`delta_${dataset}`])
        }

        // are we in for per-million values?
        if (population_ratio) {
            // we've set population ratio before already
            to_chart.data = to_chart.data.map(row => Math.round(row / population_ratio))
        }

        // are we doing the moving average thing?
        var avg_over = document.querySelector('input[type=number][name=chart-average]').value
        if ( (document.querySelector('input[type=radio][name=chart-cases]:checked').value === 'new') && (avg_over > 1) ) {
            var half_avg_over = Math.floor(avg_over/2)

            to_chart.data = to_chart.data.reduce((acc, cur, idx, arr) => {

                var mean = 0;
                var avg_start = idx - half_avg_over
                var avg_end = idx + half_avg_over

                // if we don't have enough previous data to average over
                // use whatever we have
                if (idx < half_avg_over) {
                    avg_start = 0
                }

                // if we don't have enough followinf data to average over
                // use whatever we have
                if (idx >= (arr.length - half_avg_over)) {
                    avg_end = arr.length - 1
                }

                // calculate the mean
                for (var j=avg_start; j<=avg_end; j++) {
                    mean += arr[j];
                }
                // if we're talking case fatality rate, we should not round
                if (dataset === 'cfr') {
                    acc.push(mean / (avg_end - avg_start + 1))
                // everything else nicely rounded
                } else {
                    acc.push(Math.round(mean / (avg_end - avg_start + 1)))
                }
                return acc
            }, [])
        }

        // assign the data to the chart
        theChart.data.new_datasets[siteSelect.siteNo] = to_chart

        // if we have some undefined elements in the chart's data array
        // that means that not all data is in yet -- bail!
        if (theChart.data.new_datasets.includes(undefined)) {
            return;
        }

        // update the chart settings
        clearTimeout(theChart.updateTimeout)
        theChart.updateTimeout = setTimeout(updateChartSettings, 100)
    }
}


/*
 * update various chart settings
 * based on datasets we have
 */
let updateChartSettings = () => {

    // make sure to cancel any other updates to the chart
    clearTimeout(theChart.updateTimeout)
    theChart.updateTimeout = false

    // if *any* of the sitesSelects are still fetching data, bail out
    for (let sselect of sitesSelects) {
        if (sselect.fetching) {
            console.log(`+-- data update still in progress for site: ${sselect.siteNo}`)
            return false;
        }
    }

    // which dataset are we using? confirmed, recovered, deaths, active, cfr?
    var chart_dataset = document.querySelector('input[type=radio][name=chart-data]:checked').value

    // get the y-axis type from the controls
    // for datasets active and cfr, deltas can only be graphed on linear scale, since they can (and will) get negative
    if ( (document.querySelector('input[type=radio][name=chart-cases]:checked').value === "new") && ( (chart_dataset === 'cfr') || (chart_dataset === 'active') ) ) {
        theChart.options.scales.yAxes[0].type = "linear"
    // for everything else, just heed the user setting
    } else {
        theChart.options.scales.yAxes[0].type = document.querySelector('input[type=radio][name=chart-type]:checked').value
    }

    // for cfr, start with min=0, max=1
    // also, no guidelines
    if (chart_dataset === 'cfr') {
        var max_cases = 0.01;
        var min_cases = 0;
        // make sure guidelines are hidden
        theChart.data.datasets[0].hidden = true
        theChart.data.datasets[1].hidden = true
        theChart.data.datasets[2].hidden = true

    // for everything else, a bit higher defaults
    // also, guidelines
    } else {
        // calculate the max value and the x-axis length that make sense
        // with reasonable minimums
        var max_cases = 100;
        var min_cases = 0;
        // make sure guidelines are visible
        theChart.data.datasets[0].hidden = false
        theChart.data.datasets[1].hidden = false
        theChart.data.datasets[2].hidden = false
    }

    // find the maximums and minimums
    var max_data_points = 20;
    theChart.data.new_datasets.forEach((d)=>{
        d_max = Math.max(...d.data)
        d_min = Math.min(...d.data)
        if (d_max > max_cases) {
            max_cases = d_max
        }
        if (d_min < min_cases) {
            min_cases = d_min
        }
        if (d.data.length > max_data_points) {
            max_data_points = d.data.length
        }
    })

    // set the chart factor:
    // basically, we want a bit of headspace above the highest point on any graph
    if (theChart.options.scales.yAxes[0].type == "logarithmic") {
        var chart_type_factor = 2
    } else {
        var chart_type_factor = 1.1
    }

    // set the max value
    theChart.options.scales.yAxes[0].ticks.max = Math.round((max_cases * chart_type_factor) * 1000) / 1000

    var chart_start_setting = document.querySelector('input[type=radio][name=chart-start]:checked').value
    if (chart_start_setting === "date") {
        var chart_start = 1
    } else {
        var chart_start = (1 * chart_start_setting)
    }

    // start building the title
    theChart.options.title.text = []

    // let's start with which data are we displaying, exactly
    theChart.options.scales.yAxes[0].scaleLabel.labelString = chart_dataset[0].toUpperCase() + chart_dataset.slice(1)
    if ( (chart_dataset === 'confirmed') || (chart_dataset === 'active') ) {
        theChart.options.scales.yAxes[0].scaleLabel.labelString += ' cases'
        theChart.options.title.text.push(`${chart_dataset} COVID-19 cases`)
    } else if (chart_dataset === 'recovered') {
        theChart.options.title.text.push(`COVID-19 recoveries`)
    } else if (chart_dataset === 'cfr') {
        theChart.options.scales.yAxes[0].scaleLabel.labelString = "Case fatality rate"
        theChart.options.title.text.push(`COVID-19 case fatality rate`)
    } else {
        theChart.options.title.text.push(`COVID-19 ${chart_dataset}`)
    }

    // move the y-axis zero point depending on whether we're showing cumulative or new cases
    if (document.querySelector('input[type=radio][name=chart-cases]:checked').value === 'cumulative') {
        if ( (chart_dataset !== "active" ) && (chart_dataset !== "cfr" ) && (document.querySelector('input[type=radio][name=chart-values]:checked').value === 'absolute') ) {
            theChart.options.scales.yAxes[0].ticks.min = chart_start
        } else {
            theChart.options.scales.yAxes[0].ticks.min = 0
        }

    // showing new confirmed cases / recoveries / deaths / active cases / cfr
    // notice: *new active cases* can be *negative!*
    } else {
        theChart.options.scales.yAxes[0].ticks.min = Math.round((min_cases * chart_type_factor) * 1000) / 1000
        // for confirmed, recoveries, deaths -- we're talking "new"
        if ( (chart_dataset === 'confirmed') || (chart_dataset === 'recovered') || (chart_dataset === 'deaths') ) {
            theChart.options.scales.yAxes[0].scaleLabel.labelString += ", new"
            theChart.options.title.text[0] = `new ${theChart.options.title.text[0]}`
        } else {
            theChart.options.scales.yAxes[0].scaleLabel.labelString += ", daily change"
            theChart.options.title.text[0] = `change in ${theChart.options.title.text[0]}`
        }
    }

    // amend the y-axis label
    if ( (chart_dataset != 'cfr') && (document.querySelector('input[type=radio][name=chart-values]:checked').value !== 'absolute') ) {
        theChart.options.scales.yAxes[0].scaleLabel.labelString += ', per 1 million people'
        theChart.options.title.text.push('per 1 million people')
    }

    // update x axis label
    if (chart_start_setting === "date") {
        theChart.options.scales.xAxes[0].scaleLabel.labelString = "Date"
    } else if (chart_start == 1) {
        if ( (chart_dataset != 'cfr') && (document.querySelector('input[type=radio][name=chart-values]:checked').value === 'permillion')) {
            theChart.options.scales.xAxes[0].scaleLabel.labelString = "Days since one confirmed case per million people"
        } else {
            theChart.options.scales.xAxes[0].scaleLabel.labelString = "Days since 1st confirmed case"
        }
    } else if ( (chart_dataset != 'cfr') && (document.querySelector('input[type=radio][name=chart-values]:checked').value === 'permillion') ) {
        theChart.options.scales.xAxes[0].scaleLabel.labelString = `Days since ${chart_start} confirmed cases per million people`
    } else {
        theChart.options.scales.xAxes[0].scaleLabel.labelString = `Days since ${chart_start}th confirmed case`
    }

    // show and generate the guidelines
    theChart.data.datasets[0].data = genArr(chart_start, max_data_points, (i, arr)=>{ return arr[i-1]*2 })
    theChart.data.datasets[1].data = genArr(chart_start, max_data_points, (i, arr)=>{ return arr[i-1]*Math.pow(2, 1/2) })
    theChart.data.datasets[2].data = genArr(chart_start, max_data_points, (i, arr)=>{ return arr[i-1]*Math.pow(2, 1/7) })

    // set the x-axis length
    if (chart_start_setting === "date") {
        theChart.data.labels = siteData['Global'].data.slice(-1 * theChart.data.new_datasets[0].data.length).map(d=>d.date)
    } else {
        theChart.data.labels = genArr(1, max_data_points, 1)
    }

    // add the date to the title
    theChart.options.title.text[0] = theChart.options.title.text[0][0].toUpperCase() + theChart.options.title.text[0].slice(1)
    theChart.options.title.text.push(`(as of ${siteData['Global'].data.slice(-1)[0].date.replace(/-([0-9](?![0-9]))/g, '-0$1')})`)

    // set the URL hash without firing the hashchange event
    // this handles both the sites and the settings in the URL
    updateUrlHash()

    // ignoring the potential error
    // because we might have filled out data for an nth site
    // while data for (n-1)th is still undefined
    // in which case theChart.update() will fail
    try {
        // set the new datasets as datasets
        theChart.data.new_datasets.forEach((el, idx)=>{
            theChart.data.datasets[idx + 3] = theChart.data.new_datasets[idx]
        })
        // remove any "extra" datasets
        for (var i=0; (theChart.data.new_datasets.length + 3)<theChart.data.datasets.length; i++) {
            theChart.data.datasets.pop()
        }
        // update the chart
        theChart.update();
    } catch (e) {}
}


/*
 * analyze test data
 */
let analyzeTestData = () => {
    var no_data = sites.filter((s)=>{ return ( ! ( s in siteData ) ) })
    var no_covid_data = Object.keys(siteData).filter((s)=>{ return ( ! ( "data" in siteData[s] ) ) })
    var no_population_data = Object.keys(siteData).filter((s)=>{ return ( ! ( "population" in siteData[s] ) ) })
    return {
        all: no_data,
        cases: no_covid_data,
        population: no_population_data
    }
}


/*
 * just testing stuff
 */
let testAllSites = () => {
    // covid data for all sites
    sites.slice(1).reduce((acc, cur)=>{
        return acc
            .catch((e)=>{ console.log(e) })
            .then(()=>{
                return getSiteCases(cur)
                    .catch((e)=>{ console.log(e) })
                    .then(()=>{
                        return getSitePopulation(cur)
                    })
            })
    }, getSiteCases(sites[0])
        .catch((e)=>{ console.log(e) })
        .then(()=>{
            return getSitePopulation(sites[0])
        })
    )
    .then(analyzeTestData)
}


/*
 * get an array of sites from a string
 * used mainly in the context of the URL hash
 *
 * takes a comma-separated string of URL-normalized country names
 * and returns the country names as used in siteData
 */
let getSitesFromUrlString = (sites_string) => {
    // get the non-empty site names
    hash_sites = sites_string
                    .split(',') // split by ',' in case we have a number of sites to handle
                    // filter out empty strings
                    .reduce((acc, el)=>{
                        if (el) {
                            acc.push(el);
                        }
                        return acc;
                    }, [])
    // find the sites that match those strings
    return Object.keys(siteData)
            .reduce((acc, site) => {
                // index of the site name in hash_sites
                var i = hash_sites.indexOf(site.toLowerCase().replace(/[^a-z]/g, '-'))
                if (i>=0) {
                    acc[i] = site
                }
                return acc;
            }, new Array(hash_sites.length))
            // filter out any undefined elements,
            // which are simply sites not found in siteData
            .filter(site => (site !== undefined))
}


/*
 * get sites and settings from window.location.hash
 */
let processUrlHash = (hash) => {
    // remove the "#" if present
    if (hash[0] === '#') {
        hash = hash.substr(1)
    }
    // ";" is used as a separator of options and sites
    // with sites coming last and settings being optional
    hash = hash.split(';')
    var sites_hash = hash[hash.length-1]
    var settings_hash = ''
    if (hash.length > 1) {
        settings_hash = hash[0]
    }

    // return the data
    return {
        sites: getSitesFromUrlString(sites_hash),
        // woo, sets!
        // https://wsvincent.com/javascript-remove-duplicates-array/
        settings: [...new Set(settings_hash.split(','))]
    }
}


/*
 * return array of settings that are set to something different then default
 */
let getSelectedSettings = () => {
    // get radio boxes settings
    var selected_settings = []
    document
        .querySelectorAll('.chart-config-container input[type=radio]')
        .forEach((r)=>{
            // if the state is different than default
            if (r.checked && ! r.defaultChecked) {
                selected_settings.push(r.id.split('-').slice(-1)[0])
            }
        })
    // and all other inputs too
    document
        .querySelectorAll('.chart-config-container input:not([type=radio])')
        .forEach((r)=>{
            if (r.defaultValue != r.value) {
                selected_settings.push(r.id.split('-').slice(-1)[0] + ":" + r.value)
            }
        })
    // return the array
    return selected_settings;
}

/*
 * assemble and set the url hash
 * without firing a hashchange event
 */
let updateUrlHash = () => {
    // get selected sites
    var selected_sites = []
    var valid_sites = Object.keys(siteData)
    for (let sselect of sitesSelects) {
        // this is to avoid 'select-one' showing up in the URL hash
        if (valid_sites.includes(sselect.value)) {
            selected_sites.push(sselect.value.toLowerCase().replace(/[^a-z0-9]/g, '-'))
        }
    }

    // get the settings with non-default values
    var selected_settings = getSelectedSettings()

    // using History.pushState() because we don't want the onhashchange event to fire
    var new_hash = ''
    if (selected_settings.length > 0) {
        new_hash = '#' + selected_settings.join(',') + ';' + selected_sites.join(',')
    } else {
        new_hash = '#' + selected_sites.join(',')
    }
    if (window.location.hash !== new_hash) {
        history.pushState({}, '', new_hash)
    }
}


// we need this as a global variable
var sitesSelects = null

/*
 * make sure stuff is set-up properly
 */
document.addEventListener('DOMContentLoaded', (e)=>{

    // set-up site drop-down
    sitesSelects = document.getElementsByClassName("sites-select");
    sitesSelects[0].addEventListener('change', selectSite);
    sitesSelects[0].selectTimeout = false
    sitesSelects[0].siteNo = 0;

    // now we can add more sites
    var addSite = document.getElementById('add-site')
    var removeSite = document.getElementById('remove-site')
    var clearAllSites = document.getElementById('clear-all-sites')

    // draw the chart
    chart()

    // make sure chart controls work
    document.querySelectorAll('input[type=radio][name=chart-type]').forEach((node)=>{
        node.addEventListener('change', updateChartSettings)
    })
    document.querySelectorAll('input[type=radio][name=chart-cases], input[type=radio][name=chart-start], input[type=radio][name=chart-values], input[type=number][name=chart-average], input[type=radio][name=chart-data]').forEach((node)=>{
        node.addEventListener('change', (e)=>{
            for (select_index=0; select_index<sitesSelects.length; select_index++) {
                updateChartData(sitesSelects[select_index])
            }
        })
    })
    document.querySelectorAll('input[type=date][name=chart-start-from]').forEach((node)=>{
        node.addEventListener('change', (e)=>{
            // Chrome/Chromium cannot into min/max on an input[type=date]
            if (e.target.value > e.target.max) {
                e.target.value = e.target.max
            } else if (e.target.value < e.target.min) {
                e.target.value = e.target.min
            }
            // do the thing
            for (select_index=0; select_index<sitesSelects.length; select_index++) {
                updateChartData(sitesSelects[select_index])
            }
        })
    })
    // when the user clicks on the date picker, we can assume they want to do a start-on-a-date chart
    document.querySelector('input[type=date][name=chart-start-from]').addEventListener('click', (e)=>{
        e.target.parentElement.click()
    })

    // reset settings
    document.getElementById('chart-reset-settings').addEventListener('click', (e)=>{
        // set the radio button groups to their defaults
        document
            .querySelectorAll('.chart-config-container input[type=radio]')
            .forEach((r)=>{
                if (r.defaultChecked) {
                    r.checked = true
                }
            })
        // and all other inputs to their default values
        document
            .querySelectorAll('.chart-config-container input:not([type=radio])')
            .forEach((r)=>{
                r.value = r.defaultValue
            })
        // update the chart
        for (select_index=0; select_index<sitesSelects.length; select_index++) {
            updateChartData(sitesSelects[select_index])
        }
    })


    // compare functions for sort()
    // used in "Load 6 sites..." menu buttons handler
    let menu_compare_functions = {
        "site-menu-button-rate": (a, b)=>{
            // make the formulas easier on the eyes
            var adata = siteData[a].data
            var bdata = siteData[b].data
            // get the ratios (unless already there)
            if (! ("ratio" in adata[adata.length-1]) ) {
                adata[adata.length-1].ratio = calculateAverageRate(adata.slice(-4).map(d=>d.confirmed))
            }
            if (! ("ratio" in bdata[bdata.length-1]) ) {
                bdata[bdata.length-1].ratio = calculateAverageRate(bdata.slice(-4).map(d=>d.confirmed))
            }
            // return ratio comparison
            return bdata[bdata.length-1].ratio - adata[adata.length-1].ratio;
        },
        "site-menu-button-percentage": (a, b)=>{
            // make the formulas easier on the eyes
            var adata = siteData[a].data
            var bdata = siteData[b].data
            // return confirmed-to-population ratio comparison
            return (bdata[bdata.length-1].confirmed / siteData[b].population) - (adata[adata.length-1].confirmed / siteData[a].population);
        },
        "site-menu-button-deaths": (a, b)=>{
            // make the formulas easier on the eyes
            var adata = siteData[a].data
            var bdata = siteData[b].data
            // return deaths comparison
            return bdata[bdata.length-1].deaths - adata[adata.length-1].deaths;
        },
        "site-menu-button-death-to-case": (a, b)=>{
            // make the formulas easier on the eyes
            var adata = siteData[a].data
            var bdata = siteData[b].data
            // return deaths comparison
            return (bdata[bdata.length-1].deaths / bdata[bdata.length-1].confirmed) - (adata[adata.length-1].deaths / adata[adata.length-1].confirmed);
        },
        "site-menu-button-highest-cfr": (a, b)=>{
            // make the formulas easier on the eyes
            var adata = siteData[a].data
            var bdata = siteData[b].data
            // we can assume active is there if delta_active is, right?.. right?..
            if (! ("cfr" in adata[adata.length - 1]) ) {
                if ( (adata[adata.length - 1].recovered + adata[adata.length - 1].deaths) === 0) {
                    adata[adata.length - 1].cfr = 0
                } else {
                    adata[adata.length - 1].cfr = adata[adata.length - 1].deaths / (adata[adata.length - 1].recovered + adata[adata.length - 1].deaths)
                }
            }
            if (! ("cfr" in bdata[bdata.length - 1]) ) {
                if ( (bdata[bdata.length - 1].recovered + bdata[bdata.length - 1].deaths) === 0) {
                    bdata[bdata.length - 1].cfr = 0
                } else {
                    bdata[bdata.length - 1].cfr = bdata[bdata.length - 1].deaths / (bdata[bdata.length - 1].recovered + bdata[bdata.length - 1].deaths)
                }
            }
            // return delta_active comparison
            return bdata[bdata.length-1].cfr - adata[adata.length-1].cfr;
        },
        "site-menu-button-recovered": (a, b)=>{
            // make the formulas easier on the eyes
            var adata = siteData[a].data
            var bdata = siteData[b].data
            // return deaths comparison
            return bdata[bdata.length-1].recovered - adata[adata.length-1].recovered;
        },
        "site-menu-button-active-drop": (a, b)=>{
            // make the formulas easier on the eyes
            var adata = siteData[a].data
            var bdata = siteData[b].data
            // we can assume active is there if delta_active is, right?.. right?..
            if (! ("delta_active" in adata[adata.length - 1]) ) {
                adata[adata.length - 1].active = adata[adata.length - 1].confirmed - (adata[adata.length - 1].recovered + adata[adata.length - 1].deaths)
                adata[adata.length - 2].active = adata[adata.length - 2].confirmed - (adata[adata.length - 2].recovered + adata[adata.length - 2].deaths)
                adata[adata.length - 1].delta_active = adata[adata.length - 1].active   - adata[adata.length - 2].active
            }
            if (! ("delta_active" in bdata[bdata.length - 1]) ) {
                bdata[bdata.length - 1].active = bdata[bdata.length - 1].confirmed - (bdata[bdata.length - 1].recovered + bdata[bdata.length - 1].deaths)
                bdata[bdata.length - 2].active = bdata[bdata.length - 2].confirmed - (bdata[bdata.length - 2].recovered + bdata[bdata.length - 2].deaths)
                bdata[bdata.length - 1].delta_active = bdata[bdata.length - 1].active   - bdata[bdata.length - 2].active
            }
            // return delta_active comparison
            return adata[adata.length-1].delta_active - bdata[bdata.length-1].delta_active;
        }
    }


    // chart setting functions
    // used in "Load 6 sites..." menu buttons handler
    let menu_chart_settings = {
        "site-menu-button-rate": ()=>{
            document.getElementById('chart-data-confirmed').checked = true
            document.getElementById('chart-cases-cumulative').checked = true
            document.getElementById('chart-values-absolute').checked = true
            document.getElementById('chart-type-logarithmic').checked = true
        },
        "site-menu-button-percentage": ()=>{
            document.getElementById('chart-data-confirmed').checked = true
            document.getElementById('chart-cases-cumulative').checked = true
            document.getElementById('chart-values-permillion').checked = true
            document.getElementById('chart-type-logarithmic').checked = true
        },
        "site-menu-button-deaths": ()=>{
            document.getElementById('chart-data-deaths').checked = true
            document.getElementById('chart-cases-cumulative').checked = true
            document.getElementById('chart-values-absolute').checked = true
            document.getElementById('chart-type-logarithmic').checked = true
        },
        "site-menu-button-death-to-case": ()=>{
            document.getElementById('chart-data-deaths').checked = true
            document.getElementById('chart-cases-cumulative').checked = true
            document.getElementById('chart-values-absolute').checked = true
            document.getElementById('chart-type-logarithmic').checked = true
        },
        "site-menu-button-highest-cfr": ()=>{
            document.getElementById('chart-data-cfr').checked = true
            document.getElementById('chart-cases-cumulative').checked = true
            document.getElementById('chart-values-absolute').checked = true
            document.getElementById('chart-type-linear').checked = true
        },
        "site-menu-button-recovered": ()=>{
            console.log('site-menu-button-recovered')
            document.getElementById('chart-data-recovered').checked = true
            document.getElementById('chart-cases-cumulative').checked = true
            document.getElementById('chart-values-absolute').checked = true
            document.getElementById('chart-type-logarithmic').checked = true
        },
        "site-menu-button-active-drop": ()=>{
            document.getElementById('chart-data-active').checked = true
            document.getElementById('chart-cases-delta').checked = true
            document.getElementById('chart-values-absolute').checked = true
            document.getElementById('chart-type-linear').checked = true
        }
    }


    // handle the "Load 6 sites..." menu buttons
    document.querySelectorAll('#internal-main-menu-container input[type=button]').forEach((node)=>{
        // add a listener
        node.addEventListener('click', (e)=>{
            // these should be ignored
            var ignored_sites = ['Global', 'Diamond Princess', 'MS Zaandam']
            // let's get them sites
            var sites = Object.keys(siteData)
                            // first, filter out sites without data
                            // and sites with less than 100 cases
                            .reduce((acc, site)=>{
                                // ignore the ignored sites
                                if (ignored_sites.includes(site)) {
                                    console.log(`+-- ${site} is specifically ignored; not taking into account`)
                                // ignore sites for which we have no data yet
                                } else if (! ('data' in siteData[site]) ) {
                                    console.log(`+-- no cases data for ${site}; not taking into account`)
                                // ignore sites for which we have no population yet
                                } else if (! ('population' in siteData[site]) ) {
                                    console.log(`+-- no population data for ${site}; not taking into account`)
                                // we only want sites with 100 or more confirmed cases,
                                // otherwise the data is all over the place and meaningless
                                } else if (siteData[site].data[siteData[site].data.length - 1].confirmed > 100) {
                                    acc.push(site)
                                }
                                return acc
                            }, [])
                            // sort using a function defined for that
                            .sort(menu_compare_functions[e.target.id])
                            // we only need top 6
                            .slice(0, 6)
                            // prepare for hashing
                            .map((site)=>{
                                return site.toLowerCase().replace(/[^a-z]/g, '-')
                            })
                            .join(',')

            // set the chart settings to what is best to view the particular data
            if (e.target.id in menu_chart_settings) {
                menu_chart_settings[e.target.id]()
            }
            // TODO: we need to find a better way to do this
            window.location.hash = '#' + getSelectedSettings().join(',') + ';' + sites
        })
    })

    // fill out the covid data box
    getCovidData()
        .then((data)=>{
            // update global stats
            document.querySelector('#covid-stats > #confirmed > .value').innerHTML =  data.confirmed
            document.querySelector('#covid-stats > #deaths > .value').innerHTML =  data.deaths
            document.querySelector('#covid-stats > #recovered > .value').innerHTML =  data.recovered
            document.querySelector('#covid-stats > #death-to-case > .value').innerHTML =  "~" + Math.round(data.death_to_case * 10000) / 100 + '%'

            // update the interface
            document.querySelector('input[type=date][name=chart-start-from]').min =  siteData['Global'].data[0].date.replace(/-([0-9](?![0-9]))/g, '-0$1')
            document.querySelector('input[type=date][name=chart-start-from]').value =  siteData['Global'].data[0].date.replace(/-([0-9](?![0-9]))/g, '-0$1')
            document.querySelector('input[type=date][name=chart-start-from]').max =  data.date.replace(/-([0-9](?![0-9]))/g, '-0$1')

            for (site in sites) {
                var cOpt = document.createElement("option");
                cOpt.value = sites[site];
                cOpt.innerHTML = sites[site];
                // yes, this only handles the first sites select
                // since at that point there is only one sites select
                sitesSelects[0].appendChild(cOpt);
            }
            addSite.classList.add('visible')
            addSite.addEventListener('click', (ev)=>{
                ev.preventDefault()
                var newSite = sitesSelects[0].parentElement.parentElement.cloneNode(true)
                newSite.getElementsByClassName('site-data')[0].innerHTML="";
                // append the copied node to the `#sites-data-container` element
                sitesSelects[0].parentElement.parentElement.parentElement.insertBefore(newSite, addSite.parentElement)
                // for some reason sitesSelects auto-updates and always contains the full list
                // of `.sites-select` elements. *magic*
                sitesSelects[sitesSelects.length - 1].addEventListener('change', selectSite);
                sitesSelects[sitesSelects.length - 1].siteNo = sitesSelects.length - 1
                sitesSelects[sitesSelects.length - 1].selectTimeout = false
            })
            return sites
        })
        // at this point we have all the sites ready,
        // so we can process the URL #hash
        .then(()=>{
            window.dispatchEvent(new Event('hashchange'));
        })

    removeSite.addEventListener('click', (ev)=>{
        ev.preventDefault()
        // update the chart
        if ( (theChart.data.new_datasets.length > 0) && (theChart.data.new_datasets[theChart.data.new_datasets.length - 1].label === sitesSelects[sitesSelects.length - 1].value) ) {
            theChart.data.new_datasets.pop()
        }
        // if we have more than one site available
        if (sitesSelects.length > 1) {
            // remove the site data container
            sitesSelects[sitesSelects.length - 1].parentElement.parentElement.remove()

        // ok, this is the last site, so we don't actually want to remove the whole thing
        } else {
            // instead, we want to clear it
            sitesSelects[0].parentElement.parentElement.getElementsByClassName('site-data')[0].innerHTML=""
            sitesSelects[0].value = "select one"
        }
        // using History.pushState() because we don't want the onhashchange event to fire
        updateUrlHash()

        // update the chart settings
        clearTimeout(theChart.updateTimeout)
        theChart.updateTimeout = setTimeout(updateChartSettings, 250)
    })

    clearAllSites.addEventListener('click', (e)=>{
        e.preventDefault()
        // just set the settings, with empty sites part
        // TODO: we need to do this in a better way
        window.location.hash = '#' + getSelectedSettings().join(',') + ';'
    })

    // handling manual hash change
    window.addEventListener("hashchange", (e)=>{
        console.log(`+-- manual hash change! '${window.location.hash}'`)
        console.log(`    got hash: ${window.location.hash}`)

        // get the actual site names from the URL hash (sans '#')
        var hash = processUrlHash(window.location.hash)

        // set settings
        var handled_setting_groups = []
        // handle settings from hash
        hash.settings.forEach((setting)=>{
            // do we have anything to work with?
            if (setting === '') {
                return false;
            }
            // perhaps it' a "valued" setting?
            setting = setting.split(':')
            // get the node
            var node = document.querySelector(`.chart-config-container input[id$=${setting[0]}]`)
            // if so, use .value
            if (setting.length > 1) {
                node.value = setting[1]
            // otherwise, use .checked
            } else {
                node.checked = true
            }
            // remove the node from consideration
            handled_setting_groups.push(node.name)
        })

        // set all other settings to defaults
        document
            .querySelectorAll('.chart-config-container input')
            .forEach((node)=>{
                if ( (node.name != "") && ! handled_setting_groups.includes(node.name) ) {
                    node.value = node.defaultValue
                    node.checked = node.defaultChecked
                }
            })

        // any site data containers to be removed?
        if (sitesSelects.length - hash.sites.length > 0) {
            console.log(`    +-- removing ${sitesSelects.length - hash.sites.length} sites...`)
            var cur_site_count = sitesSelects.length
            for (var i=hash.sites.length; i<cur_site_count; i++) {
                console.log(`        i: ${i}`)
                removeSite.dispatchEvent(new Event('click'))
            }
        }

        // do we have any sites to work with?
        if (hash.sites.length > 0) {
            // handle each hash site separately, adding site data containers if need be
            hash.sites.forEach((hash_site, index)=>{
                    // we only allow 6 sites
                    if ( index < 6 ) {
                        if ( ( index >= sitesSelects.length ) ) {
                            addSite.dispatchEvent(new Event('click'))
                        }
                        console.log(`    +-- got site from hash: ${hash_site}`)
                        // this only handles the first sitesSelect
                        // all others are not currently addressable in hash
                        sitesSelects[index].value = hash_site
                        sitesSelects[index].dispatchEvent(new Event('change'));
                    }
                })
        }
    }, false);
});

// https://en.wikipedia.org/wiki/File:Epidemic_curve_update_18_march_20.png
